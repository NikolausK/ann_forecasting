# ANN_Forecasting

## ANN for forecasting stock price
ANN model in PyTorch for forecasting chaotic dynamical systems.
1-D example used as a proof of concept.

## Description
Given past values/data, predict the next value.

